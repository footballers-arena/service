var express = require('express')
var router = express.Router()
const Favourites = require('../models/favourites');
const chalk = require('chalk');
const Op = require('sequelize').Op;

function getAll(req, res) {
	Favourites.findAll({
  	    attributes: ['PlayerID'],
  	    where: {
  	      Username: {
  	        [Op.eq]: req.user.user
  	      }
  	    }
  	  }).then((players) => {
		const asList = [];
		if (players) {
			for (let p in players) {
				asList.push(players[p].PlayerID);
			}
		}
  	    res.send(asList);
  	  }).catch((err) => {
  	    console.error(chalk.red("Promise Exception at Favourites GET"))
  	    console.error(err);
  	    res.status(500).send("Internal Server Error. Check server logs");
	});	
}

router.get('/', getAll);

router.put('/:footballerId', (req, res) => {
  Favourites.findOrCreate({
  	where: {
  		Username: {
  			[Op.eq]: req.user.user
  		},
  		PlayerID: {
  			[Op.eq]: parseInt(req.params.footballerId)
  		}
  	},
  	defaults: {
  		Username: req.user.user,
  		PlayerID: parseInt(req.params.footballerId)
  	}
  }).then(() => {
  	getAll(req, res);
  }).catch((err) => {
    console.error(chalk.red("Promise Exception at insert favourite"))
    console.error(err);
    res.status(500).send("Internal Server Error. Check server logs");
  });
});

router.delete('/:footballerId', (req, res) => {
  Favourites.destroy({
  	where: {
  		Username: {
  			[Op.eq]: req.user.user
  		},
  		PlayerID: {
  			[Op.eq]: parseInt(req.params.footballerId)
  		}
  	}
  }).then(() => {
  	getAll(req, res);
  }).catch((err) => {
    console.error(chalk.red("Promise Exception at delete Favourite"))
    console.error(err);
    res.status(500).send("Internal Server Error. Check server logs");
  });
});

module.exports = router;

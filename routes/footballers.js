var express = require('express')
var router = express.Router()
const Footballer = require('../models/footballers');
const chalk = require('chalk');
const Op = require('sequelize').Op

router.get('/', (req, res) => {
  Footballer.findAll({
    attributes: ['id', 'Name', 'Age', 'Nationality', 'Club', 'Rating'],
    where: {
      Name: {
        [Op.like]: `%${(req.query && req.query.filter) ? req.query.filter: ''}%`
      }
    }
  }).then((players) => {
    res.send(players);
  }).catch((err) => {
    console.error(chalk.red("Promise Exception at Footballers GET"))
    console.error(err);
    res.status(500).send("Internal Server Error. Check server logs");
  });
});

router.get('/:footballerId', (req, res) => {
  Footballer.findById(req.params.footballerId).then((player) => {
    if (player)
      res.send(player);
    else {
      res.status(404).send("No player for that Id");
    }
  }).catch((err) => {
    console.error(chalk.red("Promise Exception at Footballer By ID GET"))
    console.error(err);
    res.status(500).send("Internal Server Error. Check server logs");
  });
});

module.exports = router;

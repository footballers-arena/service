var express = require('express')
var router = express.Router()
const models = require('../models');
const path = require('path');
const chalk = require('chalk');
const footballersRoute = require('./footballers');
const favourites = require('./favourites');
const passport = require('passport')

router.use('/footballers', footballersRoute);
router.use('/favourites', favourites);

router.get('/ping', (req, res) => {
  res.send(req.user);
});


router.get('/logout', function(req, res){
  req.logout();
  //req.session.destroy();
  res.send('Logout done!');
});

module.exports = router;

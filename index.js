require('dotenv').config();
const models = require('./models');
const express = require('express');
const routes = require('./routes');
const path = require('path');
var app = express();
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const jwt = require('jsonwebtoken');
const cert = process.env.JWT_KEY;
const cors = require('cors');
const bcrypt = require('bcrypt');

passport.use(new LocalStrategy({
    usernameField: 'Username',
    passwordField: 'Password',
  }, 
  function(username, password, done) {
    models.User.findOne({ where: { Username: username } }).then(function (user) {
      if (!user) { return done(null, false); }
      return bcrypt.compare(password, user.Password).then(res => {
	      if (res)
		return done(null, { user: user.Username, name: user.Name });
	      else
		return done(null, false);
	});
    }).catch(err => {
      return done(err);
    });
  }
));

app.use(cors({
  origin: ['http://localhost:3001'], 
  credentials: true
}));

app.use(express.json())
app.use(express.urlencoded({extended: false}))
app.use(require('cookie-parser')());
app.use(require('express-session')({
  secret: process.env.EXPRESS_SESS,
  resave: false,
  saveUninitialized: true
}));

app.use(passport.initialize());
app.use(passport.session());


app.use('/', express.static(path.resolve(__dirname, './ui/build')));
app.use('/pic/', express.static(path.resolve(__dirname, './assets/footballers'), {fallthrough: true}), (req, res, next) => {
  res.sendFile(path.resolve(__dirname, './assets/anon.png'))
});


app.post('/register', (req, res) => {
    const model = models.User.build(req.body);
    model.validate()
        .then(() => {
            return model
                .save()
                .then(() => {
			passport.authenticate('local')(req, res, function() {
				res.send(req.user);
			});
		});
        })
        .catch((err) => {
            if (err.errors) {
                const processed = {};
                for (let e of err.errors) {
                    processed[e.path] = e.message;
                }
                res.status(400).send(processed);
            } else {
                res.status(400).send(err.Error);
            }
        });
});

app.post('/login', (r, R, next) => {
    next();
}, passport.authenticate('local'), (req, res) => {
  res.send(req.user)
});

app.use('/', (req, res, next) => {
  if (req.isAuthenticated()) { return next(); }
  res.status(401).send('Not logged in');
}, routes);



passport.serializeUser(function(user, done) {
  jwt.sign(user, cert, {
    expiresIn: '5h'
  }, function(err, token) {
    done(err, token);
  });
});

passport.deserializeUser(function(token, done) {
  jwt.verify(token, cert, function(err, user) {
    done(err, user);
  });
});

app.listen(process.env.PORT || 3000, () => console.log('Example app listening!'))

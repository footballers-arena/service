require('dotenv').config();
const sequelize = require('../models/connection');
const Users = require('../models/users');
const Footballers = require('../models/footballers');
const Favourites = require('../models/favourites');

const chalk = require('chalk');
console.log(chalk.red('Syncing tables'));
Favourites.sync({force: true, logging: false}).then(()=>{
  console.log(chalk.yellow('Setup Complete, Please use the dump file to load data'));
  console.log(chalk.yellow('Dump file can be run with - "mysql -u user -p database < dumpfile.sql"'));
  sequelize.close();
});

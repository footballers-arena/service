const Footballer = require('./footballers');
const User = require('./users');

module.exports = {
  Footballer,
  User
};

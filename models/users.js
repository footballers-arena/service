const Sequelize = require('sequelize');
const bcrypt = require('bcrypt');
const sequelize = require('./connection');


function setHashedPassword(inputPass) {
      if (!inputPass) return;
      const hash = bcrypt.hashSync(inputPass, Number.parseInt(process.env.BCRYPT_SALT));
      this.setDataValue('Password', hash);
}

const User = sequelize.define('users', {
    Name: {
        allowNull: false,
        defaultValue: false,
        type: Sequelize.STRING,
        validate: {
            notEmpty: true,
        }
    },
    Username: {
        allowNull: false,
        defaultValue: false,
        type: Sequelize.STRING,
        unique: true,
        validate: {
            notEmpty: true,
        }
    },
    Email: {
        allowNull: false,
        defaultValue: false,
        type: Sequelize.STRING,
        unique: true,
        validate: {
            notEmpty: true,
            isEmail: true
        }
    },
    Password: {
        allowNull: false,
        defaultValue: false,
        type: Sequelize.STRING,
 			  set: setHashedPassword,
        validate: {
            notEmpty: true,
        }
    },
}, { charset: 'utf8' });

module.exports = User;

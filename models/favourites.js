const Sequelize = require('sequelize');
const sequelize = require('./connection');

const Favourite = sequelize.define('favourites', {
    Username: {
    	references: {
    		model: 'users',
    		key: 'Username'
    	},
    	primaryKey: true,
    	type: Sequelize.STRING,
    },
    PlayerID: {
    	references: {
     		model: 'footballers',
    		key: 'id'
    	},
    	primaryKey: true,
    	type: Sequelize.INTEGER,
    }
}, { charset: 'utf8' });

module.exports = Favourite;
